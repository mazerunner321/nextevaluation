# nextevaluation

---

## Section C

## Q.1 Write and share a small note about your choice of system to schedule periodic tasks (such as downloading list of ISINs every 24 hours). Why did you choose it? Is it reliable enough; Or will it scale? If not, what are the problems with it? And, what else would you recommend to fix this problem at scale in production?

I have limited knowledge and exposure to systems available to schedule periodic tasks, but as far as I have researched about it there are many systems available majorly 1.Cron, 2.AWS CloudWatch, 3.Azure Scheduler. For simple usage, Cron is a better choice as it is widely used and simple to use. However, when the scale of the project increases, AWS CloudWatch would be a better choice as it can handle complex tasks. Therefore, it mainly depends on the usage and requirements of the project.

## Q.2 Suppose you are building a financial planning tool - which requires us to fetch bank statements from the user. Now, we would like to encrypt the data - so that nobody - not even the developer can view it. What would you do to solve this problem?

There are many ways to encrypt the data so that it is not easily visible without the required key, such as Hashing, Salting, Symmetric Encryption, Asymmetric Encryption. I have exposure to node module called bcrypt, where important data such as password or card numbers can be encrypted using hashing and telling the number of salt rounds to increase the security of the encryption. This is a type of Symmetric Encyption where the key is same for both encyrption and decryption.
