const http = require('http');
const fs = require('fs');
const path = require('path')


// create the server 
const server = http.createServer((req, res) => {
    if (req.url === '/') {
        const directoryPath = path.join("../Backend");
        const dir = fs.readdirSync(directoryPath)
        dir.map(el => res.write(`<a href=${el}><li>${el}</li></a>`))
    }
    else if (req.url === '/Applications') {
        const application = fs.readdirSync(path.join("../Backend/Applications"))
        application.map((el) => res.write(`<a href=${el}><li>${el}</li></a>\n`))
    }
    else if (req.url === '/Desktop') {
        const desktop = fs.readdirSync(path.join("../Backend/Desktop"))
        desktop.map((el) => res.write(`<a href=${el}><li>${el}</li></a>\n`))
    }
    else if (req.url === '/Documents') {
        const documents = fs.readdirSync(path.join("../Backend/Documents"))
        documents.map((el) => res.write(`<a href=${el}><li>${el}</li></a>\n`))
    }
    else if (req.url === '/Downloads') {
        const downloads = fs.readdirSync(path.join("../Backend/Downloads"))
        downloads.map((el) => res.write(`<a href=${el}><li>${el}</li></a>\n`))
    }
    else if (req.url === '/Drivers') {
        const downloads = fs.readdirSync(path.join("../Backend/Downloads/Drivers"))
        downloads.map((el) => res.write(`<a href=${el}><li>${el}</li></a>\n`))
    }
    else if (req.url === '/newfolder') {
        fs.mkdirSync('./newfolder', (err) => {
            if (err) throw err;
            else console.log({ "msg": "new folder created" });
        })
    }
    else if (req.url === '/newfile') {
        fs.appendFileSync('./newfile.txt', 'New file appended');
    }
    else if (req.url === '/deletefolder') {
        fs.rmdirSync('./newfolder');
    }
    else if (req.url === '/deletefile') {
        fs.unlink('./newfile.txt', (err) => {
            if (err) throw err;
            else console.log({ "msg": "File has been deleted." })
        })
    }
    else if (req.url === '/rename') {
        fs.rename('./newfolder', './renameFolder', (err) => {
            if (err) throw err;
            else console.log({ "msg": "Folder has been renamed." })
        })
    }
    res.end()
})


server.listen(4500, () => console.log('Server running on port 4500'));